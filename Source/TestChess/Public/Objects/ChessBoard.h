// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ChessTypes.h"
#include "Objects/PieceSettings.h"
#include "GameFramework/Actor.h"

#include "ChessBoard.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChessGameFinished, EChessGameResult, Result);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTurnSwitched, bool, IsWhiteTurn);


USTRUCT()
struct TESTCHESS_API FPieceComponentRelation
{
	GENERATED_BODY()

	UPROPERTY()
	class UPieceBase* Piece;
	
	UPROPERTY()
	UStaticMeshComponent* MeshComponent;
};


UCLASS()
class TESTCHESS_API AChessBoard : public AActor
{
	GENERATED_BODY()

	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Component)
	class UMoveGeneratorComponent* MoveGeneratorComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Component)
	class UChessAIComponent* ChessAIComponent;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	class UDataTable* PieceSettings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	class UStaticMesh* MovePreviewMesh;

	// Mesh that is used to preview moves where you can capture enemy piece
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	class UStaticMesh* MoveCapturePreviewMesh;
	
	// Mesh that is used to preview moves where you can capture enemy piece
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	class UStaticMesh* SelectionMesh;
	
	// Mesh that is used to preview moves where you can capture enemy piece
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	class UStaticMesh* ThreatMesh;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	float Scale;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Board)
	float TimeAnimation;
	
	UPROPERTY(BlueprintAssignable)
	FOnChessGameFinished ChessGameFinished;

	UPROPERTY(BlueprintAssignable)
	FOnTurnSwitched TurnSwitched;
	

private:
	UPROPERTY()
	TMap<EPieceType, FPieceSettings> PieceSettingsMap;

	UPROPERTY()
	TMap<int32, class UPieceBase*> BoardInfo;

	UPROPERTY()
	class UPieceBase* SelectedPiece;

	UPROPERTY()
	class UPieceBase* WhiteKing;
	
	UPROPERTY()
	class UPieceBase* BlackKing;
	
	UPROPERTY()
	TArray<UStaticMeshComponent*> MovePreviews;
	
	UPROPERTY()
	TArray<UStaticMeshComponent*> ThreatMeshes;
	
	UPROPERTY()
	TArray<FChessMoveAnimation> MoveAnimations;

	// why is there no Stack for UE4?
	UPROPERTY()
	TArray<FChessMove> History;

	UPROPERTY()
	TArray<FPieceComponentRelation> PieceComponentRelation;

	
		
public:	
	AChessBoard();


public:
	UFUNCTION(BlueprintCallable)
    void BoardReset();
	
	UFUNCTION(BlueprintPure, Category = "ChessStatics")
	FIntPoint PointToBoardGrid(const FVector& Point) const;

	UFUNCTION()
    void MovePiece(FChessMove Move);

	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsWhiteTurn() const { return bIsWhiteTurn; };
	

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	
public:
	void Select(const FIntPoint& Point);
	void Deselect();

	void GenerateAnimationMove(const FChessMove& Move);

	// Move pieces but without updating visual
	EChessGameResult PushMove(const FChessMove& Move);
	// Undo last move
	void PopMove();

	const TMap<int32, class UPieceBase*>& GetBoardInfo() const;
	const TArray<FChessMove>& GetHistory() const;
	class UPieceBase* GetPointInfo(const FIntPoint& Point) const;
	
	FORCEINLINE bool CanCastleWhite() const { return bCanCastleWhite; }
	FORCEINLINE bool CanCastleBlack() const { return bCanCastleBlack; }
	FORCEINLINE bool CanCastleLongWhite() const { return bCanCastleLongWhite; }
	FORCEINLINE bool CanCastleLongBlack() const { return bCanCastleLongBlack; }
	FORCEINLINE class UPieceBase* GetWhiteKing() const { return WhiteKing; }
	FORCEINLINE class UPieceBase* GetBlackKing() const { return BlackKing; }
	
	
private:
	void InitializeBoard();
	void UpdatePieces(UPieceBase* IgnorePiece = nullptr);
	void UpdateBoard(const EChessGameResult Result, const FChessMove& Move);

	void AddPiece(const FIntPoint& Point, EPieceType Type, bool IsWhite);
	void SelectPiece(UPieceBase* Piece);
	void ScanForKings();
	
	void Castle(const FChessMove& Move);
	void UnCastle(const FChessMove& Move);
	
	void Promote(const FChessMove& Move);
	void UnPromote(const FChessMove& Move);

	void ShowThreatPreview(const TArray<FIntPoint>& Threats);
	void HideThreatPreview();

	void ShowMovePreview(const TArray<FChessMove>& Moves);
	void HideMovePreview();

	UStaticMeshComponent* CreateStaticMesh(UStaticMesh* StaticMesh);
	UStaticMeshComponent* GetPieceMeshComponent(class UPieceBase* Piece) const;
	void RemoveMeshArray(TArray<UStaticMeshComponent*>& MeshArray);
	

private:
	bool bIsWhiteTurn;
	
	bool bCanCastleWhite;
	bool bCanCastleBlack;
	bool bCanCastleLongWhite;
	bool bCanCastleLongBlack;
};
