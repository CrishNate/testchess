﻿#pragma once

#include "UObject/Interface.h"
#include "IChessLevelInterface.generated.h"



UINTERFACE(Blueprintable)
class TESTCHESS_API UIChessLevelInterface : public UInterface
{
	GENERATED_BODY()
};

class TESTCHESS_API IIChessLevelInterface
{
	GENERATED_BODY()


public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	class ACameraActor* GetWhiteCamera() const;
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    class ACameraActor* GetBlackCamera() const;
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    class AChessBoard* GetBoard() const;
};
