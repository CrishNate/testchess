﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Pieces/PieceBase.h"
#include "PieceKnight.generated.h"

/**
 * 
 */
UCLASS()
class TESTCHESS_API UPieceKnight : public UPieceBase
{
	GENERATED_BODY()


public:
	virtual void GetLegalMoves(class UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves) override;

	FORCEINLINE virtual EPieceType GetPieceType() const override { return EPieceType::Knight; }
};
