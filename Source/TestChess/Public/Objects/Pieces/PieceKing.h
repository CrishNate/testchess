﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Pieces/PieceBase.h"
#include "PieceKing.generated.h"


UCLASS()
class TESTCHESS_API UPieceKing : public UPieceBase
{
	GENERATED_BODY()

	
public:
	UPieceKing();

	
public:
	virtual void GetLegalMoves(class UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves) override;
	virtual bool GenerateMoveCastle(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove);

	FORCEINLINE virtual EPieceType GetPieceType() const override { return EPieceType::King; }


private:
	bool CheckLineForObsticles(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, const FIntPoint& Delta);
};
