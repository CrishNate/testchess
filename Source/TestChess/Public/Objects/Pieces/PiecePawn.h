﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Pieces/PieceBase.h"
#include "PiecePawn.generated.h"


UCLASS()
class TESTCHESS_API UPiecePawn : public UPieceBase
{
	GENERATED_BODY()
	

public:
	virtual void GetLegalMoves(class UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves) override;
	virtual bool GenerateMove(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove) override;

	FORCEINLINE virtual EPieceType GetPieceType() const override { return EPieceType::Pawn; }


private:
	void CheckEnemySide(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& Delta, TArray<FChessMove>& OutLegalMoves);
	void CheckEnPassant(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& Delta, TArray<FChessMove>& OutLegalMoves);

	bool GenerateMoveEnPassant(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove);
	bool GenerateMoveBlockable(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove);
};
