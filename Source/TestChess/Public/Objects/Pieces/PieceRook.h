﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Objects/ChessBoard.h"
#include "Objects/Pieces/PieceBase.h"
#include "PieceRook.generated.h"

/**
 * 
 */
UCLASS()
class TESTCHESS_API UPieceRook : public UPieceBase
{
	GENERATED_BODY()


public:
	virtual void GetLegalMoves(class UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves) override;
	virtual bool GenerateMove(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove) override;

	FORCEINLINE virtual EPieceType GetPieceType() const override { return EPieceType::Rook; }


private:
	bool CastlingAvailable;
	bool CastlingLongAvailable;
	bool IsFarRook;
	bool IsCloseRook;
};
