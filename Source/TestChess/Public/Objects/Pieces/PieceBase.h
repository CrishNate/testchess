// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ChessTypes.h"

#include "PieceBase.generated.h"


UCLASS(Abstract)
class TESTCHESS_API UPieceBase : public UObject
{
	GENERATED_BODY()

	
public:
	UPieceBase();


public:
	virtual void GetLegalMoves(class UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves);
	virtual bool GenerateMove(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove);

	FORCEINLINE virtual EPieceType GetPieceType() const { return EPieceType::None; }
	

public:
	void Init(const FIntPoint& InPoint, bool InIsWhite);

	FORCEINLINE bool IsOpponent(const UPieceBase* Piece) const { return Piece->IsWhite() != bIsWhite; }
	FORCEINLINE bool IsOpponent(bool InIsTurnWhite) const { return InIsTurnWhite != bIsWhite; }
	FORCEINLINE bool IsWhite() const { return bIsWhite; }


public:
	FIntPoint Point;

	
private:
	bool bIsWhite;
};
