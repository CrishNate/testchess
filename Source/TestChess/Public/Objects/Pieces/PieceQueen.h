﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Pieces/PieceBase.h"
#include "PieceQueen.generated.h"

/**
 * 
 */
UCLASS()
class TESTCHESS_API UPieceQueen : public UPieceBase
{
	GENERATED_BODY()


public:
	FORCEINLINE virtual EPieceType GetPieceType() const override { return EPieceType::Queen; }
};
