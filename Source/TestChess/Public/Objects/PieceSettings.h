﻿#pragma once

#include "Engine/DataTable.h"
#include "ChessTypes.h"
#include "PieceSettings.generated.h"



USTRUCT(BlueprintType)
struct FPieceSettings : public FTableRowBase
{
	GENERATED_BODY()


public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Piece")
	class UStaticMesh* Body;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Object")
	UMaterialInterface* White;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Object")
	UMaterialInterface* Black;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Object")
	EPieceType PieceType;
};
