﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "ChessTypes.h"
#include "Components/ActorComponent.h"

#include "MoveGeneratorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTCHESS_API UMoveGeneratorComponent : public UActorComponent
{
	GENERATED_BODY()
	
	
private:
	UPROPERTY()
	class AChessBoard* Board;
	
	UPROPERTY()
	TArray<FChessMove> LegalMoves;

	UPROPERTY()
	TArray<FIntPoint> Threats;
	
	UPROPERTY()
	// Protector Point and its available Delta
	TMap<FIntPoint, FIntPoint> Protectors;
	
	UPROPERTY()
	FIntPoint KingCheckedByKnight;
	

public:
	UMoveGeneratorComponent(const FObjectInitializer& ObjectInitializer);


protected:
	virtual void BeginPlay() override;


public:
	EChessGameResult GenerateAllMoves();

	/*
	* Checks the point for threats
	* Not suitable for Knights
	*/
	bool CheckForThreat(FIntPoint Point, TArray<FIntPoint>& OutThreats);

	const TArray<FChessMove> GetLegalMoves() const;
	TArray<FChessMove> GetLegalMovesFor(class UPieceBase* Piece) const;
	const TArray<FIntPoint>& GetThreats() const;
	
	bool CanPointResolveCheck(const FIntPoint& Point) const;
	bool IfProtectorCanMove(const FIntPoint& Point, const FIntPoint& Delta) const;
	bool HasMove(class UPieceBase* Piece, const FIntPoint& Point, FChessMove& OutMove) const;
	bool HasMove(const FChessMove& InMove) const;

	FORCEINLINE class AChessBoard* GetBoard() const { return Board; }
	FORCEINLINE bool IsKingUnderCheck() const { return bKingUnderCheck; }

	
protected:
	/*
	* Checks basically if it's possible from a square to attack to a specific direction
	* Not suitable for Knights
	*/
	bool IsPossibleToAttack(const FIntPoint& FromSquare, FIntPoint Direction);
	bool HasDirection(const TArray<FIntPoint>& Deltas, FIntPoint Direction);

	/*
	* Checks is out king on check or not
	* @return Amount of threats we currently have on our king
	*/
	SIZE_T CheckForCheck();

	void Reset();


private:
	bool bKingUnderCheck;
};
