﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ChessTypes.h"
#include "Components/ActorComponent.h"
#include "ChessAIComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMoveFoundDelegate, FChessMove, Move);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTCHESS_API UChessAIComponent : public UActorComponent
{
	GENERATED_BODY()

	
public:
	UPROPERTY()
	FOnMoveFoundDelegate OnMoveFound;

	UPROPERTY()
	FChessMove FoundMove;
	

private:
	/*
	* Sets a depth at witch AI will looks for move
	* The more we set value, the more deeply AI looking for moves in cost of time
	*/
	UPROPERTY(EditAnywhere, Category = "AI")
	int Depth;
	
	UPROPERTY()
	class AChessBoard* Board;


public:
	UChessAIComponent(const FObjectInitializer& ObjectInitializer);


public:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	
public:
	static void DebugPrintBoard(const AChessBoard* Board, bool Different = false);
	
	void RootMiniMaxAsync();
	FChessMove RootMiniMax() const;

	FORCEINLINE class AChessBoard* GetBoard() const { return Board; }
	

private:
	// Evaluate board value at current state
	float Evaluate() const;
	float MiniMax(SIZE_T InDepth, bool IsMax, float Alpha, float Beta) const;
	
	static const TArray<float>& PieceToBestPoints(EPieceType PieceType);


private:
	static const TMap<EPieceType, float> PieceScoresMap;
	
	static const TArray<float> BestPointForPawn;
	static const TArray<float> BestPointForKnight;
	static const TArray<float> BestPointForBishop;
	static const TArray<float> BestPointForRook;
	static const TArray<float> BestPointForQueen;
	static const TArray<float> BestPointForKing;
};


class MinimaxThreadTask : public FNonAbandonableTask
{
public:
	MinimaxThreadTask(UChessAIComponent* InAIComponent);
	~MinimaxThreadTask();

	friend class FAutoDeleteAsyncTask<MinimaxThreadTask>;


public:
	void DoWork() const;

	FORCEINLINE TStatId GetStatId() const { RETURN_QUICK_DECLARE_CYCLE_STAT(MoveThreadTask, STATGROUP_ThreadPoolAsyncTasks); }


private:

	UChessAIComponent* ChessAIComponent;
};