// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Player/ChessPlayer.h"

#include "TestChessGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TESTCHESS_API ATestChessGameMode : public AGameMode
{
	GENERATED_BODY()


public:
	ATestChessGameMode(const FObjectInitializer& ObjectInitializer);


public:
	virtual void StartMatch() override;
	virtual bool ReadyToStartMatch_Implementation() override;
};
