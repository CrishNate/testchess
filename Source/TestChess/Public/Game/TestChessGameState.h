// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"


#include "ChessTypes.h"
#include "GameFramework/GameState.h"
#include "TestChessGameState.generated.h"


UCLASS()
class TESTCHESS_API ATestChessGameState : public AGameState
{
	GENERATED_BODY()


private:
	UPROPERTY()
	TArray<class UChessPlayerBase*> ChessPlayers;


public:
	ATestChessGameState(const FObjectInitializer& ObjectInitializer);


public:
	void MovePiece(class AChessPlayerState* PlayerState, FChessMove Move);
	
	
public:
	/** Called when the state transitions to InProgress */
	virtual void HandleMatchHasStarted() override;

	
public:
	void AssignPlayerColor();
	void SetViewTargetColor();
	void CreateChessPlayer(class AChessPlayerState* PlayerState);
};
