// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ChessPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TESTCHESS_API AChessPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	UFUNCTION()
    void OnPressClick();

	UFUNCTION()
    void OnTouch(ETouchIndex::Type FingerIndex, FVector Location);
	

public:
	AChessPlayerController(const FObjectInitializer& ObjectInitializer);
	

public:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;


private:
	void HandleClick(const FHitResult& Hit);
};
