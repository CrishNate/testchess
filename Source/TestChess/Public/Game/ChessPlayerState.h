// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "ChessPlayerState.generated.h"


UCLASS()
class TESTCHESS_API AChessPlayerState : public APlayerState
{
	GENERATED_BODY()


public:
	UPROPERTY(BlueprintReadOnly, Replicated)
	bool IsWhite;
};
