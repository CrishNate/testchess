﻿#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "ChessTypes.h"
#include "Objects/Pieces/PieceBase.h"

#include "ChessStatics.generated.h"


UCLASS(BlueprintType)
class TESTCHESS_API UChessStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()


public:
	UChessStatics(const FObjectInitializer& ObjectInitializer);

	
public:
	UFUNCTION(BlueprintPure, Category = "ChessStatics")
	static bool IsPointInBoard(const FIntPoint& Point);

#pragma region LevelInterfaceRead
	
	UFUNCTION(BlueprintPure, Category = "ChessStatics|Level", meta = (WorldContext = "WorldContextObject"))
    static TScriptInterface<class IIChessLevelInterface> GetChessLevelInterface(const UObject* WorldContextObject, bool& Success);
    
	UFUNCTION(Category = "ChessStatics|Level", BlueprintPure, meta = (HidePin = "WorldContextObject", WorldContext = "WorldContextObject"))
    static UObject* FindLevelScriptObject(const UObject* WorldContextObject, UClass* ObjectClass);

	UFUNCTION(Category = "ChessStatics|Level", BlueprintPure, meta = (HidePin = "WorldContextObject", WorldContext = "WorldContextObject"))
    static AActor* GetLevelScriptActor(const UObject* WorldContextObject);

	UFUNCTION(Category = "ChessStatics|Level", BlueprintPure, meta = (HidePin = "WorldContextObject", WorldContext = "WorldContextObject"))
    static AActor* GetLevelScriptActorFromStreamingLevel(const UObject* WorldContextObject, ULevelStreaming* StreamingLevel);

#pragma endregion

	
public:
	// Flip point as is it was from opponents side
	static FIntPoint FlipPoint(const FIntPoint& Point);
	static TSubclassOf<UPieceBase> GetPieceClassFromType(EPieceType PieceType);
	static const TArray<FIntPoint>& GetDeltasFromPiece(EPieceType PieceType);
	
	static int32 PointToIndex(const FIntPoint& Point);
	static FIntPoint IndexToPoint(int32 Index);
	
	
public:
	static const FIntPoint DeltaU;
	static const FIntPoint DeltaD;
	static const FIntPoint DeltaL;
	static const FIntPoint DeltaR;
	static const FIntPoint DeltaUL;
	static const FIntPoint DeltaUR;
	static const FIntPoint DeltaDL;
	static const FIntPoint DeltaDR;
	
	static const TArray<FIntPoint> DeltaFileRank;
	static const TArray<FIntPoint> DeltaLShape;
	static const TArray<FIntPoint> DeltaDiagonal;
	static const TArray<FIntPoint> DeltaAll;
	
	static const SIZE_T WhitePawnStartFile;
	static const SIZE_T BlackPawnStartFile;
	static const SIZE_T LongCastleDistance;
	
	static const SIZE_T BoardScale;
	static const SIZE_T BoardNum;
};