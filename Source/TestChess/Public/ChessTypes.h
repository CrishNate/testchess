﻿
#pragma once

#include "CoreMinimal.h"
#include "ChessTypes.generated.h"


UENUM(BlueprintType)
enum class EPieceMoveType : uint8 
{
	None		= 0,
	Ordinary	= 1,
	EnPassant	= 2,
    Promotion	= 3,
    Castle		= 4,
    CastleLong	= 5
};

UENUM(BlueprintType)
enum class EChessGameResult : uint8
{
	None		= 0,
	Check		= 1,
    Mate		= 2,
    Draw		= 3
};

UENUM(BlueprintType, meta = (Bitflags))
enum class ECancelCastleType : uint8 
{
	None				= 0,
	CancelCastle		= 1,
	CancelCastleLong	= 2,
	CancelCastleAll		= CancelCastle | CancelCastleLong,
};
ENUM_CLASS_FLAGS(ECancelCastleType);


USTRUCT(BlueprintType)
struct TESTCHESS_API FChessMove
{
	GENERATED_BODY()

	
	FChessMove()
		: Piece(nullptr)
		, Capture(nullptr)
		, PieceCastle(nullptr)
		, EnPassantCapturable(false)
		, CancelCastle(ECancelCastleType::None)
		, Type(EPieceMoveType::None)
	{ }

	UPROPERTY(BlueprintReadOnly)
	FIntPoint From;
	UPROPERTY(BlueprintReadOnly)
	FIntPoint To;

	UPROPERTY(BlueprintReadOnly)
	class UPieceBase* Piece;

	UPROPERTY(BlueprintReadOnly)
	bool EnPassantCapturable;
	
	UPROPERTY(BlueprintReadOnly)
	class UPieceBase* Capture;
	
	UPROPERTY(BlueprintReadOnly)
	class UPieceBase* PieceCastle;

	UPROPERTY(BlueprintReadOnly)
	EPieceMoveType Type;

	UPROPERTY(BlueprintReadOnly)
	ECancelCastleType CancelCastle;

	
	bool operator==(const FChessMove& Move) const
	{
		return From != Move.From
            && To != Move.To
            && Piece != Move.Piece
            && EnPassantCapturable != Move.EnPassantCapturable
            && Capture != Move.Capture
            && PieceCastle != Move.PieceCastle
            && Capture != Move.Capture
            && PieceCastle != Move.PieceCastle
            && Type != Move.Type
            && CancelCastle != Move.CancelCastle;
	}
};

USTRUCT(BlueprintType)
struct TESTCHESS_API FChessMoveAnimation
{
	GENERATED_BODY()


	FChessMoveAnimation()
	{ }

	FChessMove Move;
	float Time;
	float StartTime;

	UPROPERTY()
	UStaticMeshComponent* MeshComponent;
};

UENUM(BlueprintType)
enum class EPieceType : uint8 
{
	None	= 0,
	Pawn	= 1,
	Rook	= 2,
	Queen	= 3,
	Bishop	= 4,
	Knight	= 5,
	King	= 6,
};