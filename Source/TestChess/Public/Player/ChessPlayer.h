﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ChessPlayerBase.h"
#include "ChessPlayer.generated.h"


UCLASS()
class TESTCHESS_API UChessPlayer : public UChessPlayerBase
{
	GENERATED_BODY()


public:
	class APlayerState* PlayerState;
};
