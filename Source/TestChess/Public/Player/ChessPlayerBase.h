﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "ChessPlayerBase.generated.h"


UCLASS()
class TESTCHESS_API UChessPlayerBase : public UObject
{
	GENERATED_BODY()


public:
	UFUNCTION()
    void HandleSwitchTurn(bool IsWhite);

	
public:
	virtual void OnMoveAvailable();


public:
	void Init();
	
	
public:
	bool bIsWhite;
};
