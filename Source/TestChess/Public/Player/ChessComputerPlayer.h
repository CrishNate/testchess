﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ChessPlayerBase.h"
#include "ChessComputerPlayer.generated.h"


UCLASS()
class TESTCHESS_API UChessComputerPlayer : public UChessPlayerBase
{
	GENERATED_BODY()


public:
	virtual void OnMoveAvailable() override;
};
