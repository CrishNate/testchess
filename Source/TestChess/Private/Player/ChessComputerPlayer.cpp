﻿
#include "Player/ChessComputerPlayer.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/ChessAIComponent.h"
#include "Objects/Map/IChessLevelInterface.h"


void UChessComputerPlayer::OnMoveAvailable()
{
	bool Success;
	const TScriptInterface<IIChessLevelInterface> ChessInterface = UChessStatics::GetChessLevelInterface(GetWorld(), Success);

	if (Success)
	{
		AChessBoard* Board = IIChessLevelInterface::Execute_GetBoard(ChessInterface.GetObject());
		Board->ChessAIComponent->RootMiniMaxAsync();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Level Interface couldn't be found"));
	}
}
