﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/ChessPlayerBase.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Map/IChessLevelInterface.h"


void UChessPlayerBase::OnMoveAvailable()
{ }

void UChessPlayerBase::Init()
{
	bool Success;
	const TScriptInterface<IIChessLevelInterface> ChessInterface = UChessStatics::GetChessLevelInterface(GetWorld(), Success);

	if (Success)
	{
		AChessBoard* Board = IIChessLevelInterface::Execute_GetBoard(ChessInterface.GetObject());
		Board->TurnSwitched.AddDynamic(this, &UChessPlayerBase::HandleSwitchTurn);
    }
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Level Interface couldn't be found"));
	}

	if (bIsWhite)
	{
		OnMoveAvailable();
	}
}

void UChessPlayerBase::HandleSwitchTurn(bool IsWhite)
{
	if (IsWhite == bIsWhite)
	{
		OnMoveAvailable();
	}
}
