// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestChess.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TestChess, "TestChess" );
