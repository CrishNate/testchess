// Copyright Epic Games, Inc. All Rights Reserved.


#include "Game/TestChessGameMode.h"

#include "Game/ChessPlayerController.h"
#include "Game/ChessPlayerState.h"
#include "Game/TestChessGameState.h"


ATestChessGameMode::ATestChessGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	DefaultPawnClass = nullptr;
	GameStateClass = ATestChessGameState::StaticClass();
	PlayerControllerClass = AChessPlayerController::StaticClass();
	PlayerStateClass = AChessPlayerState::StaticClass();
}

void ATestChessGameMode::StartMatch()
{
	Super::StartMatch();
}

bool ATestChessGameMode::ReadyToStartMatch_Implementation()
{
	// if it's standalone then we playing against the bot
	if (GetNetMode() == ENetMode::NM_Standalone)
		return true;

	if (GetMatchState() == MatchState::WaitingToStart && NumPlayers == 2)
		return true;
	
	return false;
}

