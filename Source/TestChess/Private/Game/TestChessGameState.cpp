// Copyright Epic Games, Inc. All Rights Reserved.

#include "Game/TestChessGameState.h"

#include "Camera/CameraActor.h"
#include "Kismet/KismetSystemLibrary.h"

#include "ChessStatics.h"
#include "Game/ChessPlayerState.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/MoveGeneratorComponent.h"
#include "Objects/Map/IChessLevelInterface.h"
#include "Player/ChessComputerPlayer.h"
#include "Player/ChessPlayer.h"


ATestChessGameState::ATestChessGameState(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{ }

void ATestChessGameState::MovePiece(class AChessPlayerState* PlayerState, FChessMove Move)
{
	bool Success;
	const TScriptInterface<IIChessLevelInterface> ChessInterface = UChessStatics::GetChessLevelInterface(GetWorld(), Success);

	if (Success)
	{
		AChessBoard* Board = IIChessLevelInterface::Execute_GetBoard(ChessInterface.GetObject());
		if (Board->MoveGeneratorComponent->HasMove(Move) && PlayerState->IsWhite == Board->IsWhiteTurn())
		{
			Board->MovePiece(Move);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Level Interface couldn't be found"));
	}
}

void ATestChessGameState::HandleMatchHasStarted()
{
	Super::HandleMatchHasStarted();

	AssignPlayerColor();
	SetViewTargetColor();
}

void ATestChessGameState::AssignPlayerColor()
{
	if (!HasAuthority())
		return;

	const bool IsWhite = FMath::RandBool();
	AChessPlayerState* FirstPlayer = Cast<AChessPlayerState>(PlayerArray[0]);
	FirstPlayer->IsWhite = IsWhite;
	CreateChessPlayer(FirstPlayer);
	
	// Set opposite color for the second player
	if (PlayerArray.Num() > 1)
	{
		AChessPlayerState* SecondPlayer = Cast<AChessPlayerState>(PlayerArray[1]);
		SecondPlayer->IsWhite = !IsWhite;
		CreateChessPlayer(SecondPlayer);
	}
	else
	{
		// Set second player as bot
		UChessComputerPlayer* PlayerComputer = NewObject<UChessComputerPlayer>(this);
		PlayerComputer->bIsWhite = !IsWhite;
		PlayerComputer->Init();
		ChessPlayers.Add(PlayerComputer);
	}
}

void ATestChessGameState::SetViewTargetColor()
{
	// Only on clients side or when in standalone
	if (UKismetSystemLibrary::IsDedicatedServer(this))
		return;

	// Get camera perspective for each player
	bool Success;
	const TScriptInterface<IIChessLevelInterface> ChessInterface = UChessStatics::GetChessLevelInterface(GetWorld(), Success);

	if (Success)
	{
		ACameraActor* WhitePlayerCamera = IIChessLevelInterface::Execute_GetWhiteCamera(ChessInterface.GetObject());
		ACameraActor* BlackPlayerCamera = IIChessLevelInterface::Execute_GetBlackCamera(ChessInterface.GetObject());

		const auto PlayerState = GetWorld()->GetFirstPlayerController()->GetPlayerState<AChessPlayerState>();
		ACameraActor* Camera = PlayerState->IsWhite ? WhitePlayerCamera : BlackPlayerCamera;
		
		Cast<APlayerController>(GetWorld()->GetFirstPlayerController())->SetViewTarget(Camera);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Level Interface couldn't be found"));
	}
}

void ATestChessGameState::CreateChessPlayer(AChessPlayerState* PlayerState)
{
	UChessPlayer* PlayerHuman = NewObject<UChessPlayer>(this);
	PlayerHuman->PlayerState = PlayerState;
	PlayerHuman->bIsWhite = PlayerState->IsWhite;
	PlayerHuman->Init();
    ChessPlayers.Add(PlayerHuman);
}
