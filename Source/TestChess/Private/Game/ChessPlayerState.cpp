// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/ChessPlayerState.h"
#include "Net/UnrealNetwork.h"


void AChessPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps( OutLifetimeProps );
	
	DOREPLIFETIME(AChessPlayerState, IsWhite);
}
