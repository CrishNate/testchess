// Fill out your copyright notice in the Description page of Project Settings.

#include "Game/ChessPlayerController.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Map/IChessLevelInterface.h"


AChessPlayerController::AChessPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{ }

void AChessPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	bShowMouseCursor = true;
}

void AChessPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	
	InputComponent->BindAction("Click", IE_Pressed, this, &AChessPlayerController::OnPressClick);
	InputComponent->BindTouch(IE_Pressed, this, &AChessPlayerController::OnTouch);
}

void AChessPlayerController::HandleClick(const FHitResult& Hit)
{
	if (!Hit.bBlockingHit)
		return;
	
	// Get camera perspective for each player
	bool Success;
	const TScriptInterface<IIChessLevelInterface> ChessInterface = UChessStatics::GetChessLevelInterface(GetWorld(), Success);

	if (Success)
	{
		AChessBoard* Board = IIChessLevelInterface::Execute_GetBoard(ChessInterface.GetObject());
		Board->Select(Board->PointToBoardGrid(Hit.Location));
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Level Interface couldn't be found"));
	}
}

void AChessPlayerController::OnPressClick()
{
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_WorldDynamic, false, Hit);
	HandleClick(Hit);
}

void AChessPlayerController::OnTouch(ETouchIndex::Type FingerIndex, FVector Location)
{
	FHitResult Hit;
	GetHitResultUnderFinger(FingerIndex, ECC_WorldDynamic , false, Hit);
	HandleClick(Hit);
}
