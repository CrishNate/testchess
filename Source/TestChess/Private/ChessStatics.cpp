﻿#include "ChessStatics.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Engine/LevelStreaming.h"
#include "Engine/LevelScriptActor.h"

#include "Objects/Pieces/PiecePawn.h"
#include "Objects/Pieces/PieceBishop.h"
#include "Objects/Pieces/PieceKing.h"
#include "Objects/Pieces/PieceKnight.h"
#include "Objects/Pieces/PieceQueen.h"
#include "Objects/Pieces/PieceRook.h"

#include "Objects/Map/IChessLevelInterface.h"


UChessStatics::UChessStatics(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{ }

bool UChessStatics::IsPointInBoard(const FIntPoint& Point)
{
	return Point.X >= 0 && Point.Y >= 0 && Point.X < BoardScale && Point.Y < BoardScale;
}

FIntPoint UChessStatics::FlipPoint(const FIntPoint& Point)
{
	return FIntPoint(Point.X, BoardScale - 1 - Point.Y);
}

TSubclassOf<UPieceBase> UChessStatics::GetPieceClassFromType(EPieceType PieceType)
{
	switch (PieceType)
	{
	case EPieceType::Pawn:
		return UPiecePawn::StaticClass();
	case EPieceType::Rook:
		return UPieceRook::StaticClass();
	case EPieceType::Queen:
		return UPieceQueen::StaticClass();
	case EPieceType::Bishop:
		return UPieceBishop::StaticClass();
	case EPieceType::Knight:
		return UPieceKnight::StaticClass();
	case EPieceType::King:
		return UPieceKing::StaticClass();
	default:
		return nullptr;
	}
}

const TArray<FIntPoint>& UChessStatics::GetDeltasFromPiece(EPieceType PieceType)
{
	switch (PieceType)
	{
	case EPieceType::Rook:
		return DeltaFileRank;
	case EPieceType::Bishop:
		return DeltaDiagonal;
	case EPieceType::Knight:
		return DeltaLShape;
	case EPieceType::Queen:
	case EPieceType::King:
		return DeltaAll;
	}
	return DeltaAll;
}

int32 UChessStatics::PointToIndex(const FIntPoint& Point)
{
	return Point.X + Point.Y * BoardScale;
}

FIntPoint UChessStatics::IndexToPoint(int32 Index)
{
	return FIntPoint(Index % BoardScale, Index / BoardScale);
}

TScriptInterface<IIChessLevelInterface> UChessStatics::GetChessLevelInterface(const UObject* WorldContextObject, bool& Success)
{
    WorldContextObject->GetWorld()->GetLevelScriptActor();
    UObject* LevelScriptObject = FindLevelScriptObject(WorldContextObject, UIChessLevelInterface::StaticClass());
    Success = IsValid(LevelScriptObject);
    return LevelScriptObject;
}

UObject* UChessStatics::FindLevelScriptObject(const UObject* WorldContextObject, UClass* ObjectClass)
{
    UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::ReturnNull);
    if (!IsValid(World))
        return nullptr;

    AActor* LevelScriptActor = GetLevelScriptActor(WorldContextObject);
    if (UKismetSystemLibrary::DoesImplementInterface(LevelScriptActor, ObjectClass))
        return LevelScriptActor;

    for (ULevelStreaming* Sublevel : World->GetStreamingLevels())
    {
        AActor* SublevelScriptActor = GetLevelScriptActorFromStreamingLevel(WorldContextObject, Sublevel);
        if (UKismetSystemLibrary::DoesImplementInterface(SublevelScriptActor, ObjectClass))
            return SublevelScriptActor;
    }

    return nullptr;
}

AActor* UChessStatics::GetLevelScriptActor(const UObject* WorldContextObject)
{
    UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::ReturnNull);
    if (!IsValid(World))
        return nullptr;

    ULevel* Level = World->GetLevel(0);
    if (!IsValid(Level))
        return nullptr;

    AActor* LevelScriptActor = Level->GetLevelScriptActor();
    if (!IsValid(LevelScriptActor))
        return nullptr;

    return LevelScriptActor;
}

AActor* UChessStatics::GetLevelScriptActorFromStreamingLevel(const UObject* WorldContextObject, ULevelStreaming* StreamingLevel)
{
	if (!IsValid(StreamingLevel))
		return nullptr;

	if (!StreamingLevel->IsLevelLoaded())
		return nullptr;

	AActor* LevelScriptActor = StreamingLevel->GetLevelScriptActor();
	if (!IsValid(LevelScriptActor))
		return nullptr;

	return LevelScriptActor;
}

const FIntPoint UChessStatics::DeltaU(0, 1);
const FIntPoint UChessStatics::DeltaD(0, -1);
const FIntPoint UChessStatics::DeltaR(1, 0);
const FIntPoint UChessStatics::DeltaL(-1, 0);
const FIntPoint UChessStatics::DeltaUL(-1, 1);
const FIntPoint UChessStatics::DeltaUR(1, 1);
const FIntPoint UChessStatics::DeltaDL(-1, -1);
const FIntPoint UChessStatics::DeltaDR(1, -1);

const TArray<FIntPoint> UChessStatics::DeltaFileRank({DeltaU, DeltaD, DeltaR, DeltaL});
const TArray<FIntPoint> UChessStatics::DeltaLShape({{1, 2}, {1, -2}, {2, 1}, {2, -1}, {-1, 2}, {-1, -2}, {-2, 1}, {-2, -1}});
const TArray<FIntPoint> UChessStatics::DeltaDiagonal({DeltaUL, DeltaUR, DeltaDL, DeltaDR});
const TArray<FIntPoint> UChessStatics::DeltaAll({DeltaU, DeltaD, DeltaR, DeltaL, DeltaUL, DeltaUR, DeltaDL, DeltaDR});

const SIZE_T UChessStatics::WhitePawnStartFile = 1; // File 2
const SIZE_T UChessStatics::BlackPawnStartFile = 6; // File 7
const SIZE_T UChessStatics::LongCastleDistance = 4;

const SIZE_T UChessStatics::BoardScale = 8;
const SIZE_T UChessStatics::BoardNum = BoardScale * BoardScale;