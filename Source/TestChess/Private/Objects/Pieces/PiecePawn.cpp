﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Objects/Pieces/PiecePawn.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/MoveGeneratorComponent.h"


void UPiecePawn::GetLegalMoves(class UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves)
{
	const FIntPoint Delta = IsWhite() ? UChessStatics::DeltaU : UChessStatics::DeltaD;
	const SIZE_T PawnStartFile = IsWhite() ? UChessStatics::WhitePawnStartFile : UChessStatics::BlackPawnStartFile;

	if (UChessStatics::IsPointInBoard(Point + Delta)
		&& !IsValid(MoveGeneratorComponent->GetBoard()->GetPointInfo(Point + Delta))
		&& MoveGeneratorComponent->IfProtectorCanMove(Point, Delta))
	{
		// just move forward at one square
		FChessMove Move;
		if (GenerateMoveBlockable(MoveGeneratorComponent, Point + Delta, Move))
			OutLegalMoves.Add(Move);

		// First pawn move up to 2 squares
		if (Point.Y == PawnStartFile)
		{
			FChessMove MoveTwo;
			if (GenerateMoveBlockable(MoveGeneratorComponent, Point + Delta * 2, MoveTwo))
			{
				MoveTwo.EnPassantCapturable = true;
				OutLegalMoves.Add(MoveTwo);
			}
		}
	}

	// Check for enemy pieces
	CheckEnemySide(MoveGeneratorComponent, UChessStatics::DeltaR, OutLegalMoves);
	CheckEnemySide(MoveGeneratorComponent, UChessStatics::DeltaL, OutLegalMoves);

	// If there is no move so there is no En Passant move
	if (MoveGeneratorComponent->GetBoard()->GetHistory().Num() == 0)
		return;
	
	CheckEnPassant(MoveGeneratorComponent, UChessStatics::DeltaR, OutLegalMoves);
	CheckEnPassant(MoveGeneratorComponent, UChessStatics::DeltaL, OutLegalMoves);
}

void UPiecePawn::CheckEnemySide(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& Delta, TArray<FChessMove>& OutLegalMoves)
{
	const FIntPoint DeltaAttack = Delta + (IsWhite() ? UChessStatics::DeltaU : UChessStatics::DeltaD);
	if (!MoveGeneratorComponent->IfProtectorCanMove(Point, DeltaAttack))
		return;
	
	const FIntPoint NewPoint = Point + DeltaAttack;
	if (!UChessStatics::IsPointInBoard(NewPoint))
		return;
	
	if (const UPieceBase* Piece = MoveGeneratorComponent->GetBoard()->GetPointInfo(NewPoint))
	{
		if (IsOpponent(Piece))
		{
			FChessMove Move;
			if (GenerateMove(MoveGeneratorComponent, NewPoint, Move))
				OutLegalMoves.Add(Move);
		}
	}
}

void UPiecePawn::CheckEnPassant(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& Delta,
	TArray<FChessMove>& OutLegalMoves)
{
	const FChessMove& LastMove = MoveGeneratorComponent->GetBoard()->GetHistory().Last();
	const bool EnPassantAvailable = LastMove.EnPassantCapturable;
	if (!EnPassantAvailable)
		return;
	
	const FIntPoint DeltaAttack = Delta + (IsWhite() ? UChessStatics::DeltaU : UChessStatics::DeltaD);
	if (!MoveGeneratorComponent->IfProtectorCanMove(Point, DeltaAttack))
		return;
	
	const FIntPoint NewPoint = Point + Delta;
	if (!UChessStatics::IsPointInBoard(Point + DeltaAttack))
		return;
	
	if (const UPieceBase* Piece = MoveGeneratorComponent->GetBoard()->GetPointInfo(NewPoint))
	{
		// We can only capture piece if it have moved only once
		if (IsOpponent(Piece) && LastMove.Piece == Piece)
		{
			FChessMove Move;
			if (GenerateMoveEnPassant(MoveGeneratorComponent, NewPoint, Move))
				OutLegalMoves.Add(Move);
		}
	}
}

bool UPiecePawn::GenerateMoveEnPassant(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove)
{
	const FIntPoint DeltaForward = IsWhite() ? UChessStatics::DeltaU : UChessStatics::DeltaD;
	const bool Result = GenerateMove(MoveGeneratorComponent, To, OutMove);

	OutMove.To += DeltaForward;
	OutMove.Type = EPieceMoveType::EnPassant;
	return Result;
}

bool UPiecePawn::GenerateMoveBlockable(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove)
{
	if (IsValid(MoveGeneratorComponent->GetBoard()->GetPointInfo(To)))
		return false;
	
	return GenerateMove(MoveGeneratorComponent, To, OutMove);
}

bool UPiecePawn::GenerateMove(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove)
{
	bool Result = Super::GenerateMove(MoveGeneratorComponent, To, OutMove);
	const int32 EndPiecePointY = IsWhite() ? UChessStatics::BoardScale - 1 : 0;

	// If pawn reach end point it promotes to other piece
	if (To.Y == EndPiecePointY)
	{
		OutMove.Type = EPieceMoveType::Promotion;
	}

	return Result;
}