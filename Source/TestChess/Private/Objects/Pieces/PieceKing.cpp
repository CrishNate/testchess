﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Objects/Pieces/PieceKing.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/MoveGeneratorComponent.h"


UPieceKing::UPieceKing()
{ }

void UPieceKing::GetLegalMoves(UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves)
{
	bool CastlingAvailable = IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleWhite()
						 || !IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleBlack();
	bool CastlingLongAvailable = IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleLongWhite()
							 || !IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleLongBlack();
	
	for (const FIntPoint& Delta : UChessStatics::GetDeltasFromPiece(EPieceType::King))
	{
		FIntPoint NewPoint = Point + Delta;
		if (!UChessStatics::IsPointInBoard(NewPoint))
			continue;
		
		FChessMove Move;
		if (!GenerateMove(MoveGeneratorComponent, NewPoint, Move))
			continue;

		TArray<FIntPoint> Threats;
		if (!MoveGeneratorComponent->CheckForThreat(NewPoint, Threats))
		{
			// If we move anywhere and castling still available we disable it
			Move.CancelCastle = ECancelCastleType::CancelCastleAll;
			
			OutLegalMoves.Add(Move);
		}
	}

	// We can't castle when our king is under a check
	if (MoveGeneratorComponent->IsKingUnderCheck())
		return;

	if (CastlingAvailable)
	{
		// Check for short castling
		if (CheckLineForObsticles(MoveGeneratorComponent, FIntPoint(UChessStatics::BoardScale - 1, Point.Y), UChessStatics::DeltaR))
		{
			FChessMove Move;
			if (GenerateMoveCastle(MoveGeneratorComponent, Point + UChessStatics::DeltaR * (UChessStatics::LongCastleDistance - 1), Move))
			{
				OutLegalMoves.Add(Move);
			}
		}
	}
	
	if (CastlingLongAvailable)
	{
		// Check for long castling
		if (CheckLineForObsticles(MoveGeneratorComponent, FIntPoint(0, Point.Y), UChessStatics::DeltaL))
		{
			FChessMove Move;
			if (GenerateMoveCastle(MoveGeneratorComponent, Point + UChessStatics::DeltaL * UChessStatics::LongCastleDistance, Move))
			{
				OutLegalMoves.Add(Move);
			}
		}
	}
}

bool UPieceKing::GenerateMoveCastle(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove)
{
	// TODO: Fix UnCastle (For some reason it can't properly UnCastle, and sometimes castle available when it is not)
	if (!UChessStatics::IsPointInBoard(To))
		return false;
		
	// Basic Castling
	UPieceBase* Piece = MoveGeneratorComponent->GetBoard()->GetPointInfo(To);
	
	if (Piece && Piece->GetPieceType() == EPieceType::Rook)
	{
		const bool IsLongCastle = FMath::Abs((To - Point).X) == UChessStatics::LongCastleDistance;
		OutMove.From = Point;
		// we need this so king will be near the rook and not at the same square
		OutMove.To = To + (IsLongCastle ? UChessStatics::DeltaR * 2 : UChessStatics::DeltaL);
		
		OutMove.Piece = this;
		OutMove.PieceCastle = Piece;
		OutMove.CancelCastle = ECancelCastleType::CancelCastleAll;

		OutMove.Type = IsLongCastle ? EPieceMoveType::CastleLong : EPieceMoveType::Castle;

		return true;
	}
	
	return false;
}

bool UPieceKing::CheckLineForObsticles(UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, const FIntPoint& Delta)
{
	// TODO: Fix UnCastle (For some reason it can't properly UnCastle, and sometimes castle available when it is not)
	if (!UChessStatics::IsPointInBoard(To))
		return false;
	
	SIZE_T Distance = 0;
	for (FIntPoint NewPoint = Point + Delta; NewPoint != To; NewPoint += Delta)
	{
		Distance++;

		if (!UChessStatics::IsPointInBoard(NewPoint))
			return false;

		// We only need to check threats in 2 near points
		TArray<FIntPoint> Threats;
		if (Distance <= 2 && MoveGeneratorComponent->CheckForThreat(NewPoint, Threats))
			return false;
		
		const UPieceBase* Piece = MoveGeneratorComponent->GetBoard()->GetBoardInfo()[UChessStatics::PointToIndex(NewPoint)];
		if (IsValid(Piece))
			return false;
	}
	
	return true;
}
