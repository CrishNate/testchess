﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Objects/Pieces/PieceKnight.h"

#include "ChessStatics.h"
#include "Objects/Components/MoveGeneratorComponent.h"


void UPieceKnight::GetLegalMoves(UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves)
{
	// There is no way for knight to move whenever it is a protector
	if (!MoveGeneratorComponent->IfProtectorCanMove(Point, FIntPoint::ZeroValue))
		return;
	
	for (const FIntPoint& Delta : UChessStatics::GetDeltasFromPiece(GetPieceType()))
	{
		FIntPoint NewPoint = Point + Delta;
		if (!UChessStatics::IsPointInBoard(NewPoint))
			continue;
		
		FChessMove Move;
		if (!GenerateMove(MoveGeneratorComponent, NewPoint, Move))
			continue;

		OutLegalMoves.Add(Move);
	}
}
