// Fill out your copyright notice in the Description page of Project Settings.

#include "Objects/Pieces/PieceBase.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/MoveGeneratorComponent.h"


UPieceBase::UPieceBase()
{ }

void UPieceBase::GetLegalMoves(UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves)
{
	for (const FIntPoint& Delta : UChessStatics::GetDeltasFromPiece(GetPieceType()))
	{
		if (!MoveGeneratorComponent->IfProtectorCanMove(Point, Delta))
			continue;
			
		for (FIntPoint NewPoint = Point + Delta; UChessStatics::IsPointInBoard(NewPoint); NewPoint += Delta)
		{
			if (!UChessStatics::IsPointInBoard(NewPoint))
				break;

			FChessMove Move;
			if (GenerateMove(MoveGeneratorComponent, NewPoint, Move))
			{
				OutLegalMoves.Add(Move);
			}
			else
			{
				break;
			}

			if (Move.Capture)
				break;
		}
	}
}

void UPieceBase::Init(const FIntPoint& InPoint, bool InIsWhite)
{
	Point = InPoint;
	bIsWhite = InIsWhite;
}

bool UPieceBase::GenerateMove(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove)
{
	UPieceBase* Piece = MoveGeneratorComponent->GetBoard()->GetPointInfo(To);
	if (IsValid(Piece))
	{
		if (!IsOpponent(Piece))
			return false;
		
		OutMove.Capture = Piece;
	}
	OutMove.Type = EPieceMoveType::Ordinary;
	OutMove.From = Point;
	OutMove.To = To;
	OutMove.Piece = this;
	
	return true;
}