﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Objects/Pieces/PieceRook.h"



#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/MoveGeneratorComponent.h"

void UPieceRook::GetLegalMoves(UMoveGeneratorComponent* MoveGeneratorComponent, TArray<FChessMove>& OutLegalMoves)
{
	Super::GetLegalMoves(MoveGeneratorComponent, OutLegalMoves);

	CastlingAvailable = IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleWhite()
                    || !IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleBlack();
	CastlingLongAvailable = IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleLongWhite()
                        || !IsWhite() && MoveGeneratorComponent->GetBoard()->CanCastleLongBlack();

	IsFarRook = Point.X == 0;
	IsCloseRook = Point.X == UChessStatics::BoardScale - 1;
}

bool UPieceRook::GenerateMove(class UMoveGeneratorComponent* MoveGeneratorComponent, const FIntPoint& To, FChessMove& OutMove)
{
	const bool Result = Super::GenerateMove(MoveGeneratorComponent, To, OutMove);
	
	// there is no need to cancel castling if it was already canceled
	if (!CastlingAvailable && !CastlingLongAvailable)
		return Result;
	
	if (CastlingAvailable && IsCloseRook)
	{
		OutMove.CancelCastle = ECancelCastleType::CancelCastle;
	}
	
	if (CastlingLongAvailable && IsFarRook)
	{
		OutMove.CancelCastle = ECancelCastleType::CancelCastleLong;
	}
	
	return Result;
}
