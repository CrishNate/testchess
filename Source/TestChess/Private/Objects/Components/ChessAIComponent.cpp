﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Objects/Components/ChessAIComponent.h"


#include "ChessStatics.h"
#include "Objects/ChessBoard.h"
#include "Objects/Components/MoveGeneratorComponent.h"
#include "Objects/Pieces/PieceBase.h"


// Just some debug stuff 
void UChessAIComponent::DebugPrintBoard(const AChessBoard* Board, bool Different)
{
	FString String = "=\n";
	for (SIZE_T Index = 0; Index < Board->GetBoardInfo().Num(); Index++)
	{
		FIntPoint Point = UChessStatics::IndexToPoint(Index);
		Point.X = (UChessStatics::BoardScale - 1 - Point.X);
		SIZE_T InversedIndex = UChessStatics::PointToIndex(Point);
		
		
		UPieceBase* Piece = Board->GetBoardInfo()[InversedIndex];
		if (!IsValid(Piece))
		{
			String += ".";
		}
		else
		{
			switch (Piece->GetPieceType())
			{
			case EPieceType::Pawn: String += Piece->IsWhite() ? "P" : "p"; break;
			case EPieceType::Rook: String += Piece->IsWhite() ? "R" : "r"; break;
			case EPieceType::Queen: String += Piece->IsWhite() ? "Q" : "q"; break;
			case EPieceType::Bishop: String += Piece->IsWhite() ? "B" : "b"; break;
			case EPieceType::Knight: String += Piece->IsWhite() ? "N" : "n"; break;
			case EPieceType::King: String += Piece->IsWhite() ? "K" : "k"; break;
			}
		}
		String += " ";
			
		if (Index % UChessStatics::BoardScale == 7)
			String += "\n";
	}
	if (Different)
	{
		UE_LOG(LogTemp, Display, TEXT("%s"), *String);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *String);
	}
}


UChessAIComponent::UChessAIComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Depth = 3;
	
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
}

void UChessAIComponent::RootMiniMaxAsync()
{
	// We are starting new multi-threat task for AI so it won't freeze our game while AI doing its magic
	(new FAutoDeleteAsyncTask<MinimaxThreadTask>(this))->StartBackgroundTask();
}

void UChessAIComponent::BeginPlay()
{
	Super::BeginPlay();

	Board = Cast<AChessBoard>(GetOwner());
}

void UChessAIComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*
	* Because multi-threat object spawning is kinda buggy, we need to somehow read move result from another threat
	* This is tricky solution for this problem
	*/
	if (FoundMove.Type != EPieceMoveType::None)
	{
		OnMoveFound.Broadcast(FoundMove);
		FoundMove.Type = EPieceMoveType::None;
	}
}

float UChessAIComponent::Evaluate() const
{
	float Value = 0;
	
	for (const auto Piece : Board->GetBoardInfo())
	{
		if (IsValid(Piece.Value))
		{
			FIntPoint PointFromStart = Piece.Value->IsWhite() ? UChessStatics::FlipPoint(Piece.Value->Point) : Piece.Value->Point;
			
			Value += (PieceScoresMap[Piece.Value->GetPieceType()]
                    - PieceToBestPoints(Piece.Value->GetPieceType())[UChessStatics::PointToIndex(PointFromStart)])
                    * (Piece.Value->IsWhite() ? 1 : -1);;
		}
	}
	
	return Value;
}

FChessMove UChessAIComponent::RootMiniMax() const
{
	float Best = -MAX_FLT;	
	FChessMove BestMove;
	TArray<FChessMove> Moves = Board->MoveGeneratorComponent->GetLegalMoves();

	for (const FChessMove& Move : Moves)
	{
		Board->PushMove(Move);
		const float Value = FMath::Max(Best, MiniMax(Depth - 1, false /* Now Min */, -MAX_FLT, MAX_FLT));
		Board->PopMove();

		if (Value > Best)
		{
			Best = Value;
			BestMove = Move;
		}
	}

	return BestMove;
}

float UChessAIComponent::MiniMax(SIZE_T InDepth, bool IsMax, float Alpha, float Beta) const
{
	TArray<FChessMove> Moves = Board->MoveGeneratorComponent->GetLegalMoves();
	if (InDepth == 0 || Moves.Num() == 0)
		return Evaluate() * (IsMax ? -1 : 1);
	
	float Best = IsMax ? -MAX_FLT : MAX_FLT;

	for (const FChessMove& Move : Moves)
	{
		Board->PushMove(Move);
		const float Value =  MiniMax(InDepth - 1, !IsMax, Alpha, Beta);
		Best = IsMax ? FMath::Max(Best, Value) : FMath::Min(Best, Value);
		Board->PopMove();

		// Basic optimization for Mini-Max called Alpha-Beta pruning
		if (IsMax)
		{
			Alpha = FMath::Max(Alpha, Best);
		}
		else
		{
			Beta = FMath::Min(Beta, Best);
		}

		if (Beta <= Alpha)
			break;
	}

	return Best;
}

const TArray<float>& UChessAIComponent::PieceToBestPoints(EPieceType PieceType)
{
	switch (PieceType)
	{
	case EPieceType::Pawn:
		return BestPointForPawn;
	case EPieceType::Rook:
		return BestPointForRook;
	case EPieceType::Queen:
		return BestPointForQueen;
	case EPieceType::Bishop:
		return BestPointForBishop;
	case EPieceType::Knight:
		return BestPointForKnight;
	case EPieceType::King:
		return BestPointForKing;
	}

	return BestPointForPawn;
}

const TMap<EPieceType, float> UChessAIComponent::PieceScoresMap = {
	{EPieceType::None, 0},
	{EPieceType::Pawn, 10},
	{EPieceType::Knight, 30},
	{EPieceType::Bishop, 30},
	{EPieceType::Rook, 50},
	{EPieceType::Queen, 90},
	{EPieceType::King, 900},
};

// Copied from https://chessprogramming.wikispaces.com
const TArray<float> UChessAIComponent::BestPointForPawn = {
	0,  0,  0,  0,  0,  0,  0,  0,
   50, 50, 50, 50, 50, 50, 50, 50,
   10, 10, 20, 30, 30, 20, 10, 10,
    5,  5, 10, 25, 25, 10,  5,  5,
    0,  0,  0, 20, 20,  0,  0,  0,
    5, -5,-10,  0,  0,-10, -5,  5,
    5, 10, 10,-20,-20, 10, 10,  5,
    0,  0,  0,  0,  0,  0,  0,  0 };

const TArray<float> UChessAIComponent::BestPointForKnight = {
	-50,-40,-30,-30,-30,-30,-40,-50,
    -40,-20,  0,  0,  0,  0,-20,-40,
    -30,  0, 10, 15, 15, 10,  0,-30,
    -30,  5, 15, 20, 20, 15,  5,-30,
    -30,  0, 15, 20, 20, 15,  0,-30,
    -30,  5, 10, 15, 15, 10,  5,-30,
    -40,-20,  0,  5,  5,  0,-20,-40,
    -50,-40,-30,-30,-30,-30,-40,-50 };

const TArray<float> UChessAIComponent::BestPointForBishop = {
	-20,-10,-10,-10,-10,-10,-10,-20,
    -10,  0,  0,  0,  0,  0,  0,-10,
    -10,  0,  5, 10, 10,  5,  0,-10,
    -10,  5,  5, 10, 10,  5,  5,-10,
    -10,  0, 10, 10, 10, 10,  0,-10,
    -10, 10, 10, 10, 10, 10, 10,-10,
    -10,  5,  0,  0,  0,  0,  5,-10,
    -20,-10,-10,-10,-10,-10,-10,-20 };

const TArray<float> UChessAIComponent::BestPointForRook = {
	0,  0,  0,  0,  0,  0,  0,  0,
    5, 10, 10, 10, 10, 10, 10,  5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
   -5,  0,  0,  0,  0,  0,  0, -5,
    0,  0,  0,  5,  5,  0,  0,  0 };

const TArray<float> UChessAIComponent::BestPointForQueen = {
	-20,-10,-10, -5, -5,-10,-10,-20,
    -10,  0,  0,  0,  0,  0,  0,-10,
    -10,  0,  5,  5,  5,  5,  0,-10,
     -5,  0,  5,  5,  5,  5,  0, -5,
      0,  0,  5,  5,  5,  5,  0, -5,
    -10,  5,  5,  5,  5,  5,  0,-10,
    -10,  0,  5,  0,  0,  0,  0,-10,
    -20,-10,-10, -5, -5,-10,-10,-20 };

const TArray<float> UChessAIComponent::BestPointForKing = {
	-30,-40,-40,-50,-50,-40,-40,-30,
    -30,-40,-40,-50,-50,-40,-40,-30,
    -30,-40,-40,-50,-50,-40,-40,-30,
    -30,-40,-40,-50,-50,-40,-40,-30,
    -20,-30,-30,-40,-40,-30,-30,-20,
    -10,-20,-20,-20,-20,-20,-20,-10,
     20, 20,  0,  0,  0,  0, 20, 20,
     20, 30, 10,  0,  0, 10, 30, 20 };


MinimaxThreadTask::MinimaxThreadTask(UChessAIComponent* InAIComponent)
    : ChessAIComponent(InAIComponent)
{ }

MinimaxThreadTask::~MinimaxThreadTask()
{
	UE_LOG(LogTemp, Warning, TEXT("[MoveThreadTask]: Task Finished"));
}

void MinimaxThreadTask::DoWork() const
{
	const FChessMove Move = ChessAIComponent->RootMiniMax();
	if (Move.Type != EPieceMoveType::None)
	{
		ChessAIComponent->FoundMove = Move;	
	}
}
