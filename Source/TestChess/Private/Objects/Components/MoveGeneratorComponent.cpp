﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Objects/Components/MoveGeneratorComponent.h"

#include "ChessStatics.h"
#include "Objects/ChessBoard.h"

#include "Objects/Pieces/PieceBase.h"


UMoveGeneratorComponent::UMoveGeneratorComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{ }


void UMoveGeneratorComponent::BeginPlay()
{
	Super::BeginPlay();

	Board = Cast<AChessBoard>(GetOwner());
}

EChessGameResult UMoveGeneratorComponent::GenerateAllMoves()
{
	Reset();
	const SIZE_T ThreatCount = CheckForCheck();

	UPieceBase* King = Board->IsWhiteTurn() ? Board->GetWhiteKing() : Board->GetBlackKing();
	King->GetLegalMoves(this, LegalMoves);

	/*
	* if we have more than one threat, then the only legal moves that we can have is
	* to able to move our king in an unchecked square, if there is none then it's a checkmate
	*/
	if (ThreatCount > 1)
	{
		return EChessGameResult::Check;
	}
	
	/*
	* If king was checked by a Knight then either we have to move our king, or eliminate
	* the opponent's Knight, there no other options
	*/
	if (ThreatCount == 1 && KingCheckedByKnight != FIntPoint::NoneValue)
	{
		TArray<FIntPoint> KnightThreats;
		Threats.Add(KingCheckedByKnight);
	}

	for (SIZE_T i = 0; i < UChessStatics::BoardNum; i++)
	{
		auto Piece = Board->GetBoardInfo()[i];	
		if (Piece && !Piece->IsOpponent(Board->IsWhiteTurn()))
		{
			Piece->GetLegalMoves(this, LegalMoves);
		}
	}
	
	// Removing all points that can't resolve check to our king
	for (int32 Index = LegalMoves.Num() - 1; Index >= 0; Index--)
	{
		if (!CanPointResolveCheck(LegalMoves[Index].To) && LegalMoves[Index].Piece != King)
		{
			LegalMoves.RemoveAt(Index);
		}
	}

	// If we don't have anymore moves, we achieved victory or draw
	if (LegalMoves.Num() == 0)
	{
		// It is victory when king is under check and player have no moves
		if (bKingUnderCheck)
		{
			return EChessGameResult::Mate;
		}
		// otherwise it's draw
		else
		{
			return EChessGameResult::Draw;
		}
	}

	// If we have any threats it is probably check
	return Threats.Num() > 0 ? EChessGameResult::Check : EChessGameResult::None;
}

bool UMoveGeneratorComponent::CheckForThreat(FIntPoint Point, TArray<FIntPoint>& OutThreats)
{
    //checks if there is any threat from opponents Knights
	for (const FIntPoint& Delta : UChessStatics::GetDeltasFromPiece(EPieceType::Knight))
	{
        FIntPoint NewPoint = Point + Delta;
		if (!UChessStatics::IsPointInBoard(NewPoint))
			continue;
		
		UPieceBase* PossiblePiece = Board->GetPointInfo(NewPoint);
        if (IsValid(PossiblePiece) && PossiblePiece->IsOpponent(Board->IsWhiteTurn())
        	&& PossiblePiece->GetPieceType() == EPieceType::Knight)
        {
            OutThreats.Push(NewPoint);
        }
    }

    // Сhecking all possible directions
	for (const FIntPoint& Delta : UChessStatics::DeltaAll)
    {
        SIZE_T Depth = 0;
        for (FIntPoint NewPoint = Point + Delta; UChessStatics::IsPointInBoard(NewPoint); NewPoint += Delta)
        {
            Depth++;
        	UPieceBase* FoundPiece = Board->GetPointInfo(NewPoint);

        	// As long as it empty continue with next square
        	if (!IsValid(FoundPiece))
        		continue;

        	/*
        	* If we found our own piece you can stop, but this piece might protect our king
        	* But we need to ignore king, so king won't protect itself from threats
        	*/
        	if (!FoundPiece->IsOpponent(Board->IsWhiteTurn()))
        	{
        		if (FoundPiece->GetPieceType() == EPieceType::King)
        			continue;
        		
        		break;
        	}
        	
        	// If there is an opponent pawn or king on the next square then check if it can attack
            if ((FoundPiece->GetPieceType() == EPieceType::Pawn || FoundPiece->GetPieceType() == EPieceType::King)
            	&& FoundPiece->IsOpponent(Board->IsWhiteTurn()))
            {
                if (Depth == 1 && IsPossibleToAttack(NewPoint, Delta * -1))
                {
					OutThreats.Push(NewPoint);
                }
                break;
            }

        	/*
        	* If you reach at this point that means that you found opponents piece
        	* We have to check if it's a threat for the current square.
        	*/
            if (IsPossibleToAttack(NewPoint, Delta * -1))
            {
                OutThreats.Push(NewPoint);
            }

            // We have to break, cause there is no point continuing towards this direction
            break;
        }
    }
	
    return OutThreats.Num() > 0;
}

const TArray<FChessMove> UMoveGeneratorComponent::GetLegalMoves() const
{
	return LegalMoves;
}

TArray<FChessMove> UMoveGeneratorComponent::GetLegalMovesFor(UPieceBase* Piece) const
{
	TArray<FChessMove> OutMoves;

	for (auto& Move : LegalMoves)
	{
		if (Move.Piece == Piece)
		{
			OutMoves.Add(Move);
		}
	}

	return OutMoves;
}

const TArray<FIntPoint>& UMoveGeneratorComponent::GetThreats() const
{
	return Threats;
}

bool UMoveGeneratorComponent::CanPointResolveCheck(const FIntPoint& Point) const
{
	return Threats.Num() == 0 || Threats.Contains(Point);
}

bool UMoveGeneratorComponent::IfProtectorCanMove(const FIntPoint& Point, const FIntPoint& Delta) const
{
	const FIntPoint* Result = Protectors.Find(Point);
	if (Result)
	{
		// Protector can only move on specific direction forward and backwards 
		return *Result == Delta || *Result == Delta * -1;
	}
	// This point is NOT a protector
	return true;
}

SIZE_T UMoveGeneratorComponent::CheckForCheck()
{
	SIZE_T ThreatCount = 0;
	SIZE_T Depth = 0;

	bKingUnderCheck = false;
	
	const UPieceBase* King = Board->IsWhiteTurn() ? Board->GetWhiteKing() : Board->GetBlackKing();

	// Check for Knight deltas
	for (const FIntPoint& Delta : UChessStatics::GetDeltasFromPiece(EPieceType::Knight))
	{
		const FIntPoint NewPoint = King->Point + Delta;
		if (!UChessStatics::IsPointInBoard(NewPoint))
			continue;
		
		const UPieceBase* PossibleKnight = Board->GetPointInfo(NewPoint);
	
		if (IsValid(PossibleKnight) && PossibleKnight->IsOpponent(King)
            && PossibleKnight->GetPieceType() == EPieceType::Knight)
		{
			bKingUnderCheck = true;
			KingCheckedByKnight = NewPoint;
			ThreatCount++;
			break;
		}
	}

	 // Checking all possible directions
	for (const FIntPoint& Delta : UChessStatics::GetDeltasFromPiece(EPieceType::King))
    {
        Depth = 0;
        FIntPoint SquareOfGuardingPiece = FIntPoint::NoneValue;
		for (FIntPoint NewPoint = King->Point + Delta; UChessStatics::IsPointInBoard(NewPoint); NewPoint += Delta)
        {
            Depth++;
			
            const UPieceBase* PossiblePiece = Board->GetPointInfo(NewPoint);

            // As long as point is empty, continue with next point
            if (!IsValid(PossiblePiece))
                continue;

            // If there is pawn or king on the next point then check if it can attack
            if ((PossiblePiece->GetPieceType() == EPieceType::Pawn || PossiblePiece->GetPieceType() == EPieceType::King)
            	&& PossiblePiece->IsOpponent(King))
            {
                if (Depth == 1 && IsPossibleToAttack(NewPoint, Delta * -1))
                {
                    bKingUnderCheck = true;
                    ThreatCount++;

                	Threats.Add(NewPoint);
                }
                break;
            }

            // If we found our own piece we can stop, but this piece might protect our king
            if (!PossiblePiece->IsOpponent(King))
            {
                if (SquareOfGuardingPiece == FIntPoint::NoneValue)
                {
                    SquareOfGuardingPiece = NewPoint;
                    continue;
                }
            	
            	// But when we find our piece again there is no need to check for protector in this line
                break;
            }

            // If we reach at this point that means that we found opponents piece
            // We have to check if it's a threat for the current square.
            if (IsPossibleToAttack(NewPoint, Delta * -1))
            {
                if (SquareOfGuardingPiece == FIntPoint::NoneValue)
                {
                    ThreatCount++;
                    bKingUnderCheck = true;
                	Threats.Add(NewPoint);

                    for (FIntPoint Point = King->Point + Delta; Point != NewPoint; Point += Delta)
                    {
                		Threats.Add(Point);
                    }
                }
            	else
                {
            		Protectors.Add(SquareOfGuardingPiece, Delta);
                }
                break;
            }
            break;
        }
    }
    return ThreatCount;
}

bool UMoveGeneratorComponent::IsPossibleToAttack(const FIntPoint& FromSquare, FIntPoint Direction)
{
	UPieceBase* Piece = Board->GetPointInfo(FromSquare);
	switch (Piece->GetPieceType())
	{
	case EPieceType::King:
		return HasDirection(UChessStatics::GetDeltasFromPiece(EPieceType::King), Direction);
	case EPieceType::Queen:
		return HasDirection(UChessStatics::GetDeltasFromPiece(EPieceType::Queen), Direction);
	case EPieceType::Rook:
		return HasDirection(UChessStatics::GetDeltasFromPiece(EPieceType::Rook), Direction);
	case EPieceType::Bishop:
		return HasDirection(UChessStatics::GetDeltasFromPiece(EPieceType::Bishop), Direction);
	case EPieceType::Pawn:
		if (Piece->IsWhite())
		{
			return HasDirection(TArray<FIntPoint>() = { UChessStatics::DeltaUR, UChessStatics::DeltaUL }, Direction);
		}
		else
		{
			return HasDirection(TArray<FIntPoint>() = { UChessStatics::DeltaDR, UChessStatics::DeltaDL }, Direction);
		}
	}
	return false;
}

bool UMoveGeneratorComponent::HasDirection(const TArray<FIntPoint>& Deltas, FIntPoint Direction)
{
	return Deltas.Contains(Direction);
}

void UMoveGeneratorComponent::Reset()
{
	LegalMoves.Empty();
	Threats.Empty();
	Protectors.Empty();
	
	bKingUnderCheck = false;
	KingCheckedByKnight = FIntPoint::NoneValue;
}

bool UMoveGeneratorComponent::HasMove(UPieceBase* Piece, const FIntPoint& Point, FChessMove& OutMove) const
{
	const TArray<FChessMove> Moves = GetLegalMovesFor(Piece);
		
	for (auto& Move : Moves)
	{
		if (Move.Piece == Piece && Move.To == Point)
		{
			OutMove = Move;
			return true;
		}
	}
	
	return false;
}

bool UMoveGeneratorComponent::HasMove(const FChessMove& InMove) const
{
	for (const FChessMove& Move : LegalMoves)
	{
		if (InMove == Move)
			return true;
	}
	
	return false;
}
