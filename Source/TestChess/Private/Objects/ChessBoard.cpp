// Fill out your copyright notice in the Description page of Project Settings.

#include "Objects/ChessBoard.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

#include "ChessStatics.h"
#include "Game/ChessPlayerState.h"
#include "Objects/Components/ChessAIComponent.h"
#include "Objects/Components/MoveGeneratorComponent.h"
#include "Objects/Pieces/PieceBase.h"
#include "Objects/PieceSettings.h"


// Sets default values
AChessBoard::AChessBoard()
{
	MoveGeneratorComponent = CreateDefaultSubobject<UMoveGeneratorComponent>(TEXT("MoveGeneratorComponent"));
	ChessAIComponent = CreateDefaultSubobject<UChessAIComponent>(TEXT("ChessAIComponent"));
	ChessAIComponent->OnMoveFound.AddDynamic(this, &AChessBoard::MovePiece);

	Scale = 100.0f;
	bIsWhiteTurn = true;
	
	bCanCastleWhite = true;
	bCanCastleBlack = true;
	bCanCastleLongWhite = true;
	bCanCastleLongBlack = true;

	TimeAnimation = 1.0f;
	PrimaryActorTick.bCanEverTick = true;
}

void AChessBoard::Select(const FIntPoint& Point)
{
	if (UKismetSystemLibrary::IsDedicatedServer(this))
		return;
	// only client side beyond this point

	if (GetWorld()->GetFirstPlayerController()->GetPlayerState<AChessPlayerState>()->IsWhite != bIsWhiteTurn)
		return;

	UPieceBase* PotentialPiece = BoardInfo[UChessStatics::PointToIndex(Point)];
	if (PotentialPiece && !PotentialPiece->IsOpponent(bIsWhiteTurn))
	{
		SelectPiece(PotentialPiece);
	}
	else if (IsValid(SelectedPiece))
	{
		// we can move selected piece only when we select empty square or enemy piece
		FChessMove Move;
		if (MoveGeneratorComponent->HasMove(SelectedPiece, Point, Move))
		{
			MovePiece(Move);

			if (!HasAuthority())
			{
				MovePiece(Move);
			}
			return;
		}

		// If there is no move that is fit request, player probably pressed empty space
		Deselect();
	}
}

void AChessBoard::SelectPiece(UPieceBase* Piece)
{
	if (IsValid(SelectedPiece))
	{
		Deselect();
	}
	
	SelectedPiece = Piece;

	ShowMovePreview(MoveGeneratorComponent->GetLegalMovesFor(SelectedPiece));
}

void AChessBoard::Deselect()
{
	SelectedPiece = nullptr;
	HideMovePreview();
}

void AChessBoard::MovePiece(FChessMove Move)
{
	// remove captured piece
	if (IsValid(Move.Capture))
	{
		Move.Capture->ConditionalBeginDestroy();
		Move.Capture->MarkPendingKill();
	}
	
	const EChessGameResult Result = PushMove(Move);
	UChessAIComponent::DebugPrintBoard(this);
	
	// update clients board
	if (!UKismetSystemLibrary::IsDedicatedServer(this))
	{
		// We remove promoted pawn so it will not be rendered
		if (Move.Type == EPieceMoveType::Promotion)
		{
			Move.Piece->ConditionalBeginDestroy();
			Move.Piece->MarkPendingKill();
		}
		UpdateBoard(Result, Move);
	}

	// So If there is no moves for player it is a mate or draw
	if (Result == EChessGameResult::Mate || Result == EChessGameResult::Draw)
	{
		ChessGameFinished.Broadcast(Result);
	}
	
	TurnSwitched.Broadcast(bIsWhiteTurn);
}

void AChessBoard::BoardReset()
{
	bIsWhiteTurn = true;
	
	bCanCastleWhite = true;
	bCanCastleBlack = true;
	bCanCastleLongWhite = true;
	bCanCastleLongBlack = true;
	
	for (SIZE_T Index = 0; Index < UChessStatics::BoardNum; Index++)
	{
		auto Piece = BoardInfo[Index];

		if (IsValid(Piece))
		{
			Piece->ConditionalBeginDestroy();
			Piece->MarkPendingKill();
		}
	}
	
	InitializeBoard();
	ScanForKings();
	
	MoveGeneratorComponent->GenerateAllMoves();
}

EChessGameResult AChessBoard::PushMove(const FChessMove& Move)
{
	BoardInfo[UChessStatics::PointToIndex(Move.To)] = Move.Piece;
	Move.Piece->Point = Move.To;
	BoardInfo[UChessStatics::PointToIndex(Move.From)] = nullptr;

	switch (Move.Type)
	{
	case EPieceMoveType::Promotion:
		Promote(Move);
		break;
		
	case EPieceMoveType::Castle:
	case EPieceMoveType::CastleLong:
		Castle(Move);
		break;
	default:
		break;
	}

	if ((Move.CancelCastle & ECancelCastleType::CancelCastle) == ECancelCastleType::CancelCastle)
	{
		(bIsWhiteTurn ? bCanCastleWhite : bCanCastleBlack) = false;
	}
	
	if ((Move.CancelCastle & ECancelCastleType::CancelCastleLong) == ECancelCastleType::CancelCastleLong)
	{
		(bIsWhiteTurn ? bCanCastleLongWhite : bCanCastleLongBlack) = false;
	}
	
	History.Add(Move);
	
	bIsWhiteTurn = !bIsWhiteTurn;

	// regenerate all moves for next turn
	return MoveGeneratorComponent->GenerateAllMoves();
}

void AChessBoard::PopMove()
{
	if (History.Num() == 0)
		return;
	
	bIsWhiteTurn = !bIsWhiteTurn;
	
	auto Move = History.Last();
	History.RemoveAt(History.Num() - 1);

	if ((Move.CancelCastle & ECancelCastleType::CancelCastle) == ECancelCastleType::CancelCastle)
	{
		(bIsWhiteTurn ? bCanCastleWhite : bCanCastleBlack) = true;
	}
	
	if ((Move.CancelCastle & ECancelCastleType::CancelCastleLong) == ECancelCastleType::CancelCastleLong)
	{
		(bIsWhiteTurn ? bCanCastleLongWhite : bCanCastleLongBlack) = true;
	}

	switch (Move.Type)
	{
	case EPieceMoveType::Promotion:
		UnPromote(Move);
		break;
		
	// For En Passant Move we also need to clear point where our pawn were placed, after capture of opponent pawn
	case EPieceMoveType::EnPassant:
		BoardInfo[UChessStatics::PointToIndex(Move.To)] = nullptr;
		break;
		
	case EPieceMoveType::Castle:
    case EPieceMoveType::CastleLong:
        UnCastle(Move);
		break;
	default:
		break;
	}
	
	BoardInfo[UChessStatics::PointToIndex(Move.From)] = Move.Piece;
	Move.Piece->Point = Move.From;

	if (IsValid(Move.Capture))
	{
		// We revive our piece exactly where it was captured, because it is impossible that it moved to somewhere else
		BoardInfo[UChessStatics::PointToIndex(Move.Capture->Point)] = Move.Capture;
	}
	else
	{
		BoardInfo[UChessStatics::PointToIndex(Move.To)] = nullptr;
	}
}

void AChessBoard::GenerateAnimationMove(const FChessMove& Move)
{
	FChessMoveAnimation NewMoveAnimation;
	NewMoveAnimation.Move = Move;
	NewMoveAnimation.StartTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
	NewMoveAnimation.Time = TimeAnimation;
	NewMoveAnimation.MeshComponent = GetPieceMeshComponent(Move.Piece);

	MoveAnimations.Add(NewMoveAnimation);
}

void AChessBoard::ShowThreatPreview(const TArray<FIntPoint>& Threats)
{
	HideThreatPreview();
	
	for (const FIntPoint& Threat : Threats)
	{
		if (!IsValid(GetPointInfo(Threat)))
			continue;
		
		auto MeshComponent = CreateStaticMesh(ThreatMesh);
		MeshComponent->SetRelativeLocation(FVector(-Threat.X, Threat.Y, 0) * Scale);
		ThreatMeshes.Add(MeshComponent);
	}
}

void AChessBoard::HideThreatPreview()
{
	RemoveMeshArray(ThreatMeshes);
}

void AChessBoard::ShowMovePreview(const TArray<FChessMove>& Moves)
{
	HideMovePreview();

	auto SelectionMeshComponent = CreateStaticMesh(SelectionMesh);
	SelectionMeshComponent->SetRelativeLocation(FVector(-SelectedPiece->Point.X, SelectedPiece->Point.Y, 0) * Scale);
	MovePreviews.Add(SelectionMeshComponent);
	
	for (const FChessMove& Move : Moves)
	{
		auto MeshComponent = CreateStaticMesh(Move.Capture ? MoveCapturePreviewMesh : MovePreviewMesh);
		MeshComponent->SetRelativeLocation(FVector(-Move.To.X, Move.To.Y, 0) * Scale);
		MovePreviews.Add(MeshComponent);
	}
}

void AChessBoard::HideMovePreview()
{
	RemoveMeshArray(MovePreviews);
}

UStaticMeshComponent* AChessBoard::CreateStaticMesh(UStaticMesh* StaticMesh)
{
	UStaticMeshComponent* MeshComponent = NewObject<UStaticMeshComponent>(this);
	MeshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	MeshComponent->SetStaticMesh(StaticMesh);
	MeshComponent->RegisterComponent();
	
	return MeshComponent;
}

void AChessBoard::RemoveMeshArray(TArray<UStaticMeshComponent*>& MeshArray)
{
	for (SIZE_T i = 0; i < MeshArray.Num(); i++)
	{
		MeshArray[i]->DestroyComponent();
	}
	MeshArray.Empty();
}

UStaticMeshComponent* AChessBoard::GetPieceMeshComponent(UPieceBase* Piece) const
{
	for (const auto& Relation : PieceComponentRelation)
	{
		if (Relation.Piece == Piece)
			return Relation.MeshComponent;
	}

	return nullptr;
}

const TMap<int32, UPieceBase*>& AChessBoard::GetBoardInfo() const
{
	return BoardInfo;
}

const TArray<FChessMove>& AChessBoard::GetHistory() const
{
	return History;
}

UPieceBase* AChessBoard::GetPointInfo(const FIntPoint& Point) const
{
	return BoardInfo[UChessStatics::PointToIndex(Point)];
}

// Called when the game starts or when spawned
void AChessBoard::BeginPlay()
{
	Super::BeginPlay();

	for (SIZE_T Index = 0; Index < UChessStatics::BoardNum; Index++)
	{
		BoardInfo.Add(Index);
	}
	
	TArray<FPieceSettings*> OutSettings;
	PieceSettings->GetAllRows<FPieceSettings>(TEXT("ChessBoard"), OutSettings);
	for (FPieceSettings* Settings : OutSettings)
	{
		PieceSettingsMap.Add(Settings->PieceType, *Settings);
	}

	BoardReset();
}

void AChessBoard::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Update only for clients
	if (UKismetSystemLibrary::IsDedicatedServer(this))
		return;
	
	for (int32 Index = MoveAnimations.Num() - 1; Index >= 0; Index--)
	{
		auto MoveAnimation = MoveAnimations[Index];
		
		// Update piece move animation
		const float Delta = FMath::Min(1.0f,
			(UGameplayStatics::GetRealTimeSeconds(GetWorld()) - MoveAnimation.StartTime) / MoveAnimation.Time);

		FVector Position = static_cast<FVector>(MoveAnimation.Move.From)
            + static_cast<FVector>(MoveAnimation.Move.To - MoveAnimation.Move.From) * Delta
            + FVector::UpVector * FMath::Sin(Delta * PI);
		Position.X *= -1;
	
		if (FMath::Abs(Delta - 1.0f) < FLT_EPSILON)
			MoveAnimations.RemoveAt(Index);

		MoveAnimation.MeshComponent->SetRelativeLocation(Position * Scale);
	}
}

void AChessBoard::ScanForKings()
{
	for (SIZE_T i = 0; i < UChessStatics::BoardNum; i++)
	{
		UPieceBase* Piece = BoardInfo[i];
		if (IsValid(Piece) && Piece->GetPieceType() == EPieceType::King)
		{
			(Piece->IsWhite() ? WhiteKing : BlackKing) = Piece;
		}
	}
}

void AChessBoard::Castle(const FChessMove& Move)
{
	const FIntPoint DeltaCastle = Move.Type == EPieceMoveType::CastleLong
		                              ? UChessStatics::DeltaR
		                              : UChessStatics::DeltaL;
	
	BoardInfo[UChessStatics::PointToIndex(Move.PieceCastle->Point)] = nullptr;
	BoardInfo[UChessStatics::PointToIndex(Move.To + DeltaCastle)] = Move.PieceCastle;
	Move.PieceCastle->Point = Move.To + DeltaCastle;
}

void AChessBoard::UnCastle(const FChessMove& Move)
{
	// Getting original points of rooks before castle
	const int32 PointX = Move.Type == EPieceMoveType::CastleLong ? 0 : UChessStatics::BoardScale - 1;
	const FIntPoint Point(PointX, Move.From.Y);
	
	BoardInfo[UChessStatics::PointToIndex(Move.PieceCastle->Point)] = nullptr;
	BoardInfo[UChessStatics::PointToIndex(Point)] = Move.PieceCastle;
	Move.PieceCastle->Point = Point;
}

void AChessBoard::Promote(const FChessMove& Move)
{
	AddPiece(Move.To, EPieceType::Queen, Move.Piece->IsWhite());
}

void AChessBoard::UnPromote(const FChessMove& Move)
{
	UPieceBase* Piece = GetPointInfo(Move.To);
	Piece->ConditionalBeginDestroy();
	Piece->MarkPendingKill();
}

void AChessBoard::InitializeBoard()
{
	AddPiece(FIntPoint(0, 0), EPieceType::Rook, true);
	AddPiece(FIntPoint(1, 0), EPieceType::Knight, true);
	AddPiece(FIntPoint(2, 0), EPieceType::Bishop, true);
	AddPiece(FIntPoint(3, 0), EPieceType::Queen, true);
	AddPiece(FIntPoint(4, 0), EPieceType::King, true);
	AddPiece(FIntPoint(5, 0), EPieceType::Bishop, true);
	AddPiece(FIntPoint(6, 0), EPieceType::Knight, true);
	AddPiece(FIntPoint(7, 0), EPieceType::Rook, true);
	
	for (SIZE_T i = 0; i < UChessStatics::BoardScale; i++)
	{
		AddPiece(FIntPoint(i, 1), EPieceType::Pawn, true);
	}
	
	const SIZE_T TopSquare = UChessStatics::BoardScale - 1;
	AddPiece(FIntPoint(0, TopSquare), EPieceType::Rook, false);
	AddPiece(FIntPoint(1, TopSquare), EPieceType::Knight, false);
	AddPiece(FIntPoint(2, TopSquare), EPieceType::Bishop, false);
	AddPiece(FIntPoint(3, TopSquare), EPieceType::Queen, false);
	AddPiece(FIntPoint(4, TopSquare), EPieceType::King, false);
	AddPiece(FIntPoint(5, TopSquare), EPieceType::Bishop, false);
	AddPiece(FIntPoint(6, TopSquare), EPieceType::Knight, false);
	AddPiece(FIntPoint(7, TopSquare), EPieceType::Rook, false);
	
	for (SIZE_T i = 0; i < UChessStatics::BoardScale; i++)
	{
		AddPiece(FIntPoint(i, TopSquare - 1), EPieceType::Pawn, false);
	}
	
	UpdatePieces();
}

void AChessBoard::AddPiece(const FIntPoint& Point, EPieceType Type, bool IsWhite)
{
	const TSubclassOf<UPieceBase> PieceClass = UChessStatics::GetPieceClassFromType(Type);
	auto Piece = NewObject<UPieceBase>(this, PieceClass);
	
	Piece->Init(Point, IsWhite);
	BoardInfo[UChessStatics::PointToIndex(Point)] = Piece;
}

FIntPoint AChessBoard::PointToBoardGrid(const FVector& Point) const
{
	FVector NewPoint = UKismetMathLibrary::InverseTransformLocation(GetTransform(), Point);
	NewPoint /= Scale;
	NewPoint.X = FMath::RoundToFloat(NewPoint.X);
	NewPoint.Y = FMath::RoundToFloat(NewPoint.Y);

	const int32 X = FMath::Clamp(static_cast<int32>(NewPoint.X) * -1, 0, static_cast<int32>(UChessStatics::BoardScale) - 1);
	const int32 Y = FMath::Clamp(static_cast<int32>(NewPoint.Y), 0, static_cast<int32>(UChessStatics::BoardScale) - 1);
	return FIntPoint(X, Y);
}

void AChessBoard::UpdatePieces(UPieceBase* IgnorePiece)
{
	// Update only for clients
	if (UKismetSystemLibrary::IsDedicatedServer(this))
		return;

	for (int32 Index = PieceComponentRelation.Num() - 1; Index >= 0; Index--)
	{
		if (!IsValid(PieceComponentRelation[Index].Piece))
		{
			PieceComponentRelation[Index].MeshComponent->DestroyComponent();
			PieceComponentRelation.RemoveAt(Index);
		}
	}
	
	for (SIZE_T i = 0; i < UChessStatics::BoardNum; i++)
	{
		UPieceBase* Piece = BoardInfo[i];
		if (!IsValid(Piece))
			continue;

		if (IsValid(IgnorePiece) && Piece == IgnorePiece)
			continue;
		
		UStaticMeshComponent* MeshComponent = GetPieceMeshComponent(Piece);
		
		if (!MeshComponent)
		{
			FPieceSettings Settings = PieceSettingsMap[Piece->GetPieceType()];
			
			MeshComponent = NewObject<UStaticMeshComponent>(this);
			MeshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform); 
			MeshComponent->RegisterComponentWithWorld(GetWorld());
			MeshComponent->SetStaticMesh(Settings.Body); 
			MeshComponent->SetMaterial(0, Piece->IsWhite() ? Settings.White : Settings.Black);

			PieceComponentRelation.Add(FPieceComponentRelation{Piece, MeshComponent});
		}
	
		MeshComponent->SetRelativeLocation(FVector(-Piece->Point.X, Piece->Point.Y, 0) * Scale);
	}
}

void AChessBoard::UpdateBoard(const EChessGameResult Result, const FChessMove& Move)
{
	GenerateAnimationMove(Move);
	UpdatePieces(Move.Piece);
	HideMovePreview();

	if (Result == EChessGameResult::Check)
	{
		ShowThreatPreview(MoveGeneratorComponent->GetThreats());
	}
	else
	{
		HideThreatPreview();
	}

	SelectedPiece = nullptr;
}
